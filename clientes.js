'use strict'

const port = process.env.PORT || 3100;
const URL_WS = "http://localhost:3000/api";

const {json, response} = require('express');
const express = require('express');
const logger = require('morgan');
const fetch = require('node-fetch');
const app = express();

// Declaramos nuestros middleware
app.use(logger('dev'));
app.use(express.urlencoded({extended: false}));
app.use(express.json());

// Creamos un Middleware de Autorización tipo Bear
function auth( req, res, next ){
    if ( !req.headers.authorization ){
        res.status(401).json({
            result: 'KO',
            mensaje: "No se ha enviado el token tipo Bearer en la cabecera Authorization"
        });
        return next( new Error("Falta el token"));
    };
    const token = req.headers.authorization.split(" ")[1];
    if ( token == "MITOKEN123456789"){
        req.params.token = token;       // Creamos propiedad y propagamos el token
        return next();
    };

    res.status(401).json({
        result: 'KO',
        mensaje: "Acceso no autorizado a este servicio."
    });

    return next(new Error("Acceso no autorizado"));
}


// Declaramos nuestras rutas y nuetros controladores
app.get('/api', (req, res, next) => {
    const queURL = `${URL_WS}`;

    fetch(queURL)
        .then(resp => resp.json())
        .then(json => {
            // Lógica de negocio
            res.json({
                result: 'OK',
                colecciones: colecciones
            });
    });
});

app.get('/api/:colecciones', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queURL = `${URL_WS}/${queColeccion}`;

    fetch(queURL)
        .then(resp => resp.json())
        .then(json => {
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                elementos: json.elementos
            });
        });
});

app.get('/api/:colecciones/:id', (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const queID = req.params.id;
    const queURL = `${URL_WS}/${queColeccion}/${queID}`;

    fetch(queURL)
        .then(resp => resp.json())
        .then(json => {
            res.json({
                result: 'OK',
                coleccion: queColeccion,
                elemento: json.elemento
            });
        });
});

app.post('/api/:colecciones', auth, (req, res, next) => {
    const nuevoElemento = req.body;
    const queColeccion = req.params.colecciones;
    const queURL = `${URL_WS}/${queColeccion}`;
    const queToken = req.params.token;

    fetch(queURL, {
        method: 'POST',
        body: JSON.stringify(nuevoElemento),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${queToken}`
        }
    })
    .then(resp => resp.json())
    .then(json => {
        res.json({
            result: 'OK',
            coleccion: queColeccion,
            elemento: json.elemento
        });
    });
    
});

app.put('/api/:colecciones/:id', auth, (req, res, next) => {
    const queColeccion = req.params.colecciones;
    const elementoID = req.params.id;
    const nuevosDatos = req.body;
    const queURL = `${URL_WS}/${queColeccion}/${elementoID}`;
    const queToken = req.params.token;

    fetch(queURL, {
        method: 'PUT',
        body: JSON.stringify(nuevosDatos),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Barer ${queToken}`
        }
    })
    .then((resp) => json())
    .then(json => {
        // Logica de negocio
        res.json({
            result: 'OK',
            coleccion: queColeccion,
            resultado: json.resultado
        });
    });
});

app.delete('/api/:colecciones/:id', auth, (req, res, next) => {
    const elementoID = req.params.id;
    const queColeccion = req.params.colecciones;
    const queURL = `${URL_WS}/${queColeccion}/${elementoID}`;
    const queToken = req.params.token;

    fetch(queURL, {
        method: 'DELETE',
        body: JSON.stringify(elementoID),
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Barer ${queToken}`
        }
    })
    .then(resp => json())
    .then(json => {
        // Logica de negocio
        res.json({
            result: 'OK',
            coleccion: queColeccion,
            elementoId: elementoID,
            resultado: json.resultado
        })
    });
});

app.listen(port, () => {
    console.log(`API GW CRUD ejecutandose en http://localhost:${port}/api/{tabla}/{id}`);
});